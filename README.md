# 1. Организуем доступ к нодам

В моем случае ключи для подключения по ssh распространялись через terraform поэтому каких либо дополнитительных действий не требуется и доступы к ВМ уже есть

Так как решено было собрать кластер вручную далее для настройки всех необходимых моделуй будем использовать
следующие файлы:<br>
      - [k8s-cluster](./src/k8s-cluster.yaml)<br>
      - [prod.yaml](./src/prod.yaml)<br>
 

# 2. Настройка кластера

1. В файле `prod.yaml` вписываем адреса наших нод которые хотим собрать в кластер. Далее используем ansible-playbook `k8s-cluster` для установки необходимого ПО на нодах.

```bash

root@exe-ubuntu:/home/exe/Desktop/DIPLOM/src/ansible# ansible-playbook -i prod.yaml k8s-cluster.yaml 

PLAY [Configuration Kubernetes nodes] ***************************************************************************************************************************

TASK [Gathering Facts] ******************************************************************************************************************************************
ok: [worker-node-2]
ok: [master-node]
ok: [worker-node-1]

TASK [Turn off swap] ********************************************************************************************************************************************
changed: [master-node]
changed: [worker-node-2]
changed: [worker-node-1]

TASK [Load modules] *********************************************************************************************************************************************
changed: [master-node]
changed: [worker-node-2]
changed: [worker-node-1]

TASK [Add configuration for automatically modules loaded] *******************************************************************************************************
changed: [master-node]
changed: [worker-node-2]
changed: [worker-node-1]

TASK [Setup sysctl params] **************************************************************************************************************************************
changed: [master-node] => (item=net.bridge.bridge-nf-call-iptables)
changed: [worker-node-2] => (item=net.bridge.bridge-nf-call-iptables)
changed: [worker-node-1] => (item=net.bridge.bridge-nf-call-iptables)
changed: [worker-node-2] => (item=net.ipv4.ip_forward)
changed: [master-node] => (item=net.ipv4.ip_forward)
changed: [worker-node-1] => (item=net.ipv4.ip_forward)
changed: [master-node] => (item=net.bridge.bridge-nf-call-ip6tables)
changed: [worker-node-2] => (item=net.bridge.bridge-nf-call-ip6tables)
changed: [worker-node-1] => (item=net.bridge.bridge-nf-call-ip6tables)

TASK [Add Docker GPG apt Key] ***********************************************************************************************************************************
changed: [worker-node-2]
changed: [worker-node-1]
changed: [master-node]

TASK [Add Docker Repository] ************************************************************************************************************************************
changed: [worker-node-2]
changed: [worker-node-1]
changed: [master-node]

TASK [Install the containerd package] ***************************************************************************************************************************
changed: [master-node]
changed: [worker-node-2]
changed: [worker-node-1]

TASK [Create a configuration file for containerd and set it to default] *****************************************************************************************
changed: [master-node]
changed: [worker-node-2]
changed: [worker-node-1]

TASK [Set cgroupdriver to systemd] ******************************************************************************************************************************
changed: [master-node]
changed: [worker-node-2]
changed: [worker-node-1]

TASK [Restart containerd package] *******************************************************************************************************************************
changed: [master-node]
changed: [worker-node-2]
changed: [worker-node-1]

TASK [mkdir keyrings] *******************************************************************************************************************************************
changed: [master-node]
changed: [worker-node-2]
changed: [worker-node-1]

TASK [Add gpg key k8s] ******************************************************************************************************************************************
changed: [worker-node-2]
changed: [master-node]
changed: [worker-node-1]

TASK [Add the Kubernetes repository] ****************************************************************************************************************************
changed: [master-node]
changed: [worker-node-1]
changed: [worker-node-2]

TASK [Update Repository cache] **********************************************************************************************************************************
ok: [master-node]
ok: [worker-node-2]
ok: [worker-node-1]

TASK [Install all Kubernetes modules (kubelet, kubeadm, kubectl)] ***********************************************************************************************
changed: [worker-node-2]
changed: [master-node]
changed: [worker-node-1]

TASK [Enable kubelet] *******************************************************************************************************************************************
changed: [master-node]
changed: [worker-node-2]
changed: [worker-node-1]

PLAY RECAP ******************************************************************************************************************************************************
master-node                : ok=17   changed=15   unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
worker-node-1              : ok=17   changed=15   unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
worker-node-2              : ok=17   changed=15   unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   


```


2. Установливаем мастера `kubeadm init` на `master node`

```bash
I0616 10:09:51.977560    5606 version.go:256] remote version is much newer: v1.30.2; falling back to: stable-1.28
[init] Using Kubernetes version: v1.28.11
[preflight] Running pre-flight checks
[preflight] Pulling images required for setting up a Kubernetes cluster
[preflight] This might take a minute or two, depending on the speed of your internet connection
[preflight] You can also perform this action in beforehand using 'kubeadm config images pull'
W0616 10:10:09.743464    5606 checks.go:835] detected that the sandbox image "registry.k8s.io/pause:3.8" of the container runtime is inconsistent with that used by kubeadm. It is recommended that using "registry.k8s.io/pause:3.9" as the CRI sandbox image.
[certs] Using certificateDir folder "/etc/kubernetes/pki"
[certs] Generating "ca" certificate and key
[certs] Generating "apiserver" certificate and key
[certs] apiserver serving cert is signed for DNS names [kubernetes kubernetes.default kubernetes.default.svc kubernetes.default.svc.cluster.local node-0] and IPs [10.96.0.1 10.10.1.4]
[certs] Generating "apiserver-kubelet-client" certificate and key
[certs] Generating "front-proxy-ca" certificate and key
[certs] Generating "front-proxy-client" certificate and key
[certs] Generating "etcd/ca" certificate and key
[certs] Generating "etcd/server" certificate and key
[certs] etcd/server serving cert is signed for DNS names [localhost node-0] and IPs [10.10.1.4 127.0.0.1 ::1]
[certs] Generating "etcd/peer" certificate and key
[certs] etcd/peer serving cert is signed for DNS names [localhost node-0] and IPs [10.10.1.4 127.0.0.1 ::1]
[certs] Generating "etcd/healthcheck-client" certificate and key
[certs] Generating "apiserver-etcd-client" certificate and key
[certs] Generating "sa" key and public key
[kubeconfig] Using kubeconfig folder "/etc/kubernetes"
[kubeconfig] Writing "admin.conf" kubeconfig file
[kubeconfig] Writing "kubelet.conf" kubeconfig file
[kubeconfig] Writing "controller-manager.conf" kubeconfig file
[kubeconfig] Writing "scheduler.conf" kubeconfig file
[etcd] Creating static Pod manifest for local etcd in "/etc/kubernetes/manifests"
[control-plane] Using manifest folder "/etc/kubernetes/manifests"
[control-plane] Creating static Pod manifest for "kube-apiserver"
[control-plane] Creating static Pod manifest for "kube-controller-manager"
[control-plane] Creating static Pod manifest for "kube-scheduler"
[kubelet-start] Writing kubelet environment file with flags to file "/var/lib/kubelet/kubeadm-flags.env"
[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
[kubelet-start] Starting the kubelet
[wait-control-plane] Waiting for the kubelet to boot up the control plane as static Pods from directory "/etc/kubernetes/manifests". This can take up to 4m0s
[apiclient] All control plane components are healthy after 6.001742 seconds
[upload-config] Storing the configuration used in ConfigMap "kubeadm-config" in the "kube-system" Namespace
[kubelet] Creating a ConfigMap "kubelet-config" in namespace kube-system with the configuration for the kubelets in the cluster
[upload-certs] Skipping phase. Please see --upload-certs
[mark-control-plane] Marking the node node-0 as control-plane by adding the labels: [node-role.kubernetes.io/control-plane node.kubernetes.io/exclude-from-external-load-balancers]
[mark-control-plane] Marking the node node-0 as control-plane by adding the taints [node-role.kubernetes.io/control-plane:NoSchedule]
[bootstrap-token] Using token: 9xijw2.h67iaurlja9ffkcj
[bootstrap-token] Configuring bootstrap tokens, cluster-info ConfigMap, RBAC Roles
[bootstrap-token] Configured RBAC rules to allow Node Bootstrap tokens to get nodes
[bootstrap-token] Configured RBAC rules to allow Node Bootstrap tokens to post CSRs in order for nodes to get long term certificate credentials
[bootstrap-token] Configured RBAC rules to allow the csrapprover controller automatically approve CSRs from a Node Bootstrap Token
[bootstrap-token] Configured RBAC rules to allow certificate rotation for all node client certificates in the cluster
[bootstrap-token] Creating the "cluster-info" ConfigMap in the "kube-public" namespace
[kubelet-finalize] Updating "/etc/kubernetes/kubelet.conf" to point to a rotatable kubelet client certificate and key
[addons] Applied essential addon: CoreDNS
[addons] Applied essential addon: kube-proxy

Your Kubernetes control-plane has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

Alternatively, if you are the root user, you can run:

  export KUBECONFIG=/etc/kubernetes/admin.conf

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

Then you can join any number of worker nodes by running the following on each as root:

kubeadm join 10.10.1.4:6443 --token 9....j \
	--discovery-token-ca-cert-hash sha256:5c0977ef4d55ffcee4bf7a9134b7610147e63808a794239baed1d86754614ac0 

```

3. Настройка виртуальной сети
  ```bash
  curl -f https://raw.githubusercontent.com/projectcalico/calico/v3.26.1/manifests/calico.yaml -O
  ```
  ```bash
  kubectl apply -f calico.yaml
  ```
  В связи с введенными ограничениями на доступ к docker.io с российских ip пришлось прибегать к обходным путем заменяя все, что ссылается на `docker.io` на  `huecker.io`
  В противном случае кластер не собирался и сам контейнер с calico не переходил в `Running`
  P.S. На сегодня это неактуально... пока.


4. Подлючаем ноды к кластеру (команду для подлючения будет показана в конце установки мастера)
  ```bash
kubeadm join 10.10.1.4:6443 --token 9x...j --discovery-token-ca-cert-hash sha256:5c0977ef4d55ffcee4bf7a9134b7610147e63808a794239baed1d86754614ac0  
  ```

В конце должны получить результат с подключенным нодами:

```bash
kubectl get nodes
NAME     STATUS   ROLES           AGE     VERSION
node-0   Ready    control-plane   9m59s   v1.28.11
node-1   Ready    <none>          76s     v1.28.11
node-2   Ready    <none>          101s    v1.28.11
```

5. Проверяем, что ноды подсоединились и кластер работает
```bash
kubectl get all --all-namespaces
NAMESPACE     NAME                                           READY   STATUS    RESTARTS   AGE
kube-system   pod/calico-kube-controllers-7ddc4f45bc-clqq8   1/1     Running   0          7m53s
kube-system   pod/calico-node-76xpg                          1/1     Running   0          4m16s
kube-system   pod/calico-node-8f8qv                          1/1     Running   0          4m41s
kube-system   pod/calico-node-9cdqd                          1/1     Running   0          7m53s
kube-system   pod/coredns-5dd5756b68-7qc8v                   1/1     Running   0          12m
kube-system   pod/coredns-5dd5756b68-8bhtk                   1/1     Running   0          12m
kube-system   pod/etcd-node-0                                1/1     Running   0          12m
kube-system   pod/kube-apiserver-node-0                      1/1     Running   0          12m
kube-system   pod/kube-controller-manager-node-0             1/1     Running   0          12m
kube-system   pod/kube-proxy-862dl                           1/1     Running   0          4m16s
kube-system   pod/kube-proxy-gxdn8                           1/1     Running   0          4m41s
kube-system   pod/kube-proxy-nff2x                           1/1     Running   0          12m
kube-system   pod/kube-scheduler-node-0                      1/1     Running   0          12m

NAMESPACE     NAME                 TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)                  AGE
default       service/kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP                  12m
kube-system   service/kube-dns     ClusterIP   10.96.0.10   <none>        53/UDP,53/TCP,9153/TCP   12m

NAMESPACE     NAME                         DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR            AGE
kube-system   daemonset.apps/calico-node   3         3         3       3            3           kubernetes.io/os=linux   7m53s
kube-system   daemonset.apps/kube-proxy    3         3         3       3            3           kubernetes.io/os=linux   12m

NAMESPACE     NAME                                      READY   UP-TO-DATE   AVAILABLE   AGE
kube-system   deployment.apps/calico-kube-controllers   1/1     1            1           7m53s
kube-system   deployment.apps/coredns                   2/2     2            2           12m

NAMESPACE     NAME                                                 DESIRED   CURRENT   READY   AGE
kube-system   replicaset.apps/calico-kube-controllers-7ddc4f45bc   1         1         1       7m53s
kube-system   replicaset.apps/coredns-5dd5756b68                   2         2         2       12m
root@node-0:~# kubectl get all --all-namespaces
NAMESPACE     NAME                                           READY   STATUS    RESTARTS   AGE
kube-system   pod/calico-kube-controllers-7ddc4f45bc-clqq8   1/1     Running   0          8m5s
kube-system   pod/calico-node-76xpg                          1/1     Running   0          4m28s
kube-system   pod/calico-node-8f8qv                          1/1     Running   0          4m53s
kube-system   pod/calico-node-9cdqd                          1/1     Running   0          8m5s
kube-system   pod/coredns-5dd5756b68-7qc8v                   1/1     Running   0          13m
kube-system   pod/coredns-5dd5756b68-8bhtk                   1/1     Running   0          13m
kube-system   pod/etcd-node-0                                1/1     Running   0          13m
kube-system   pod/kube-apiserver-node-0                      1/1     Running   0          13m
kube-system   pod/kube-controller-manager-node-0             1/1     Running   0          13m
kube-system   pod/kube-proxy-862dl                           1/1     Running   0          4m28s
kube-system   pod/kube-proxy-gxdn8                           1/1     Running   0          4m53s
kube-system   pod/kube-proxy-nff2x                           1/1     Running   0          13m
kube-system   pod/kube-scheduler-node-0                      1/1     Running   0          13m

NAMESPACE     NAME                 TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)                  AGE
default       service/kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP                  13m
kube-system   service/kube-dns     ClusterIP   10.96.0.10   <none>        53/UDP,53/TCP,9153/TCP   13m

NAMESPACE     NAME                         DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR            AGE
kube-system   daemonset.apps/calico-node   3         3         3       3            3           kubernetes.io/os=linux   8m5s
kube-system   daemonset.apps/kube-proxy    3         3         3       3            3           kubernetes.io/os=linux   13m

NAMESPACE     NAME                                      READY   UP-TO-DATE   AVAILABLE   AGE
kube-system   deployment.apps/calico-kube-controllers   1/1     1            1           8m5s
kube-system   deployment.apps/coredns                   2/2     2            2           13m

NAMESPACE     NAME                                                 DESIRED   CURRENT   READY   AGE
kube-system   replicaset.apps/calico-kube-controllers-7ddc4f45bc   1         1         1       8m5s
kube-system   replicaset.apps/coredns-5dd5756b68 

```


# 3. Проблемы


В связи с ограничениями на доступ к docker.io с российских ip пришлось прибегать к обходным путем заменяя все, что ссылается на `docker.io` на  `huecker.io` в файле `calico.yaml`
В противном случае кластер не собирался и сам контейнер с calico не переходил в `Running`